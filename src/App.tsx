import React from 'react';
import CourseForm from './components/CourseForm';
function App() {
  return (
    <div className='App'>
      <div className='h-screen max-w-screen-md px-6 py-5 mx-auto'>
        <div className='flex justify-center'>
          <CourseForm />
        </div>
      </div>
    </div>
  );
}

export default App;
