/**
 * 1. Imrove the code (keep course details in one object) --- Done
 * 2. Put every valid search in a list and render it --- Done
 * 3. If input is Duplicate, throw an error
 */

import { useEffect, useState } from 'react';

interface IDepartmentAndCourse {
  department: string;
  course: string;
}
interface ISemesterAndYear {
  semester: string;
  year: string;
}

interface ICourseDetails {
  department: string;
  course: string;
  semester: string;
  year: string;
}

type Error = {
  error: boolean;
  message: string;
};
function CourseForm() {
  const [userInput, setUserInput] = useState<string>('');
  const [error, setError] = useState<Error>({ error: false, message: '' });

  //Divided in two parts because thats how we are validating and populating the input
  const [departmentAndCourse, setDepartmentAndCourse] = useState<IDepartmentAndCourse>({ department: '', course: '' });
  const [semesterAndYear, setSemesterAndYear] = useState<ISemesterAndYear>({ semester: '', year: '' });

  const [courseList, setCourseList] = useState<ICourseDetails[]>([]);

  const [isValidAndNormalised, setIsValidAndNormalised] = useState(false);

  //Possible Valid Course Strings

  // CS111 2018Fall -- 2
  // CS-111 Fall18 -- 2
  // CS-111 W18 -- 2
  // CS:111 Su2018 -- 2

  // CS111 2018 Fall -- 3
  // CS-111 Fall 2016 -- 3
  // CS:111 2016 Fall -- 3
  // Math 123 2015Spring -- 3
  // Math 123 Fall2015 -- 3
  // Math 123 F2015 -- 3

  // Math 123 2014 Summer -- 4
  // CS 120 Spring 2015 -- 4
  // CS 101 F 2016 -- 4

  const validateDepartmentAndCourse = (str1: string, str2: string = '') => {
    const regAlphabetNumber = /^[a-zA-Z]+([0-9])+$/;
    const regNumber = /^\d+$/;
    const regAlphabets = /^[a-zA-Z]+$/;

    if (regAlphabetNumber.test(str1)) {
      const num = str1.match(/\d+/g);
      const letter = str1.match(/[a-zA-Z]+/g);
      if (letter && letter[0] && num && num[0]) {
        setDepartmentAndCourse({ department: letter[0].toUpperCase(), course: num[0] });
      }
      return true;
    } else if (str1.includes('-')) {
      //ignore str2
      if (regAlphabets.test(str1.split('-')[0]) && regNumber.test(str1.split('-')[1])) {
        setDepartmentAndCourse({
          department: str1.split('-')[0].toUpperCase(),
          course: str1.split('-')[1],
        });
        return true;
      }
    } else if (str1.includes(':')) {
      //ignore str2
      if (regAlphabets.test(str1.split(':')[0]) && regNumber.test(str1.split(':')[1])) {
        setDepartmentAndCourse({
          department: str1.split(':')[0].toUpperCase(),
          course: str1.split(':')[1],
        });
        return true;
      }
    } else if (regAlphabets.test(str1) && str2 !== '' && regNumber.test(str2)) {
      setDepartmentAndCourse({ department: str1.toUpperCase(), course: str2 });
      return true;
    }
    return false;
  };

  const validateSemesterAndYear = (str1: string, str2: string = '') => {
    const reg2Or4Digit = /^(?:\d{2}|\d{4})$/;
    const regNumber = /^\d+$/;

    if (str2 === '') {
      //for length 2
      if (str1.match(/\d+/g) && str1.match(/[a-zA-Z]+/g)) {
        const num = str1.match(/\d+/g);
        const letter = str1.match(/[a-zA-Z]+/g);
        if (
          num &&
          num[0] &&
          reg2Or4Digit.test(num[0]) &&
          letter &&
          (letter[0].toLowerCase() === 'fall' ||
            letter[0].toLowerCase() === 'summer' ||
            letter[0].toLowerCase() === 'spring' ||
            letter[0].toLowerCase() === 'winter' ||
            letter[0].toLowerCase() === 'f' ||
            letter[0].toLowerCase() === 'su' ||
            letter[0].toLowerCase() === 's' ||
            letter[0].toLowerCase() === 'w')
        ) {
          if (
            (parseInt(num[0]) >= 2000 && parseInt(num[0]) <= 2099) ||
            (num[0].length === 2 && parseInt(num[0]) <= 99)
          ) {
            setSemesterAndYear({ semester: normaliseSemester(letter[0]), year: normaliseYear(num[0]) });
            return true;
          }
        }
      }
    } else if (
      reg2Or4Digit.test(str1) &&
      (str2.toLowerCase() === 'fall' ||
        str2.toLowerCase() === 'summer' ||
        str2.toLowerCase() === 'spring' ||
        str2.toLowerCase() === 'winter' ||
        str2.toLowerCase() === 'f' ||
        str2.toLowerCase() === 'su' ||
        str2.toLowerCase() === 's' ||
        str2.toLowerCase() === 'w')
    ) {
      if ((parseInt(str1) >= 2000 && parseInt(str1) <= 2099) || (str1.length === 2 && parseInt(str1) <= 99)) {
        setSemesterAndYear({ semester: normaliseSemester(str2), year: normaliseYear(str1) });
        return true;
      }
    } else if (
      reg2Or4Digit.test(str2) &&
      (str1.toLowerCase() === 'fall' ||
        str1.toLowerCase() === 'summer' ||
        str1.toLowerCase() === 'spring' ||
        str1.toLowerCase() === 'winter' ||
        str1.toLowerCase() === 'f' ||
        str1.toLowerCase() === 'su' ||
        str1.toLowerCase() === 's' ||
        str1.toLowerCase() === 'w')
    ) {
      if ((parseInt(str2) >= 2000 && parseInt(str2) <= 2099) || (str2.length === 2 && parseInt(str2) <= 99)) {
        setSemesterAndYear({ semester: normaliseSemester(str1), year: normaliseYear(str2) });
        return true;
      }
    } else if (regNumber.test(str1) && str2.match(/\d+/g) && str2.match(/[a-zA-Z]+/g)) {
      const num = str2.match(/\d+/g);
      const letter = str2.match(/[a-zA-Z]+/g);
      if (
        num &&
        num[0] &&
        reg2Or4Digit.test(num[0]) &&
        letter &&
        (letter[0].toLowerCase() === 'fall' ||
          letter[0].toLowerCase() === 'summer' ||
          letter[0].toLowerCase() === 'spring' ||
          letter[0].toLowerCase() === 'winter' ||
          letter[0].toLowerCase() === 'f' ||
          letter[0].toLowerCase() === 'su' ||
          letter[0].toLowerCase() === 's' ||
          letter[0].toLowerCase() === 'w')
      ) {
        if ((parseInt(num[0]) >= 2000 && parseInt(num[0]) <= 2099) || (num[0].length === 2 && parseInt(num[0]) <= 99)) {
          setSemesterAndYear({ semester: normaliseSemester(letter[0]), year: normaliseYear(num[0]) });
          return true;
        }
      }
    }
    return false;
  };

  const normaliseYear = (str: string) => {
    if (str.length === 2) {
      return '20' + str;
    } else {
      return str;
    }
  };
  const normaliseSemester = (str: string) => {
    let result = str;
    switch (str.toLowerCase()) {
      case 'f':
        result = 'Fall';
        break;
      case 'w':
        result = 'Winter';
        break;
      case 'su':
        result = 'Summer';
        break;
      case 's':
        result = 'Spring';
        break;
      default:
        result = str.charAt(0).toUpperCase() + str.slice(1);
    }
    return result;
  };
  const isDuplicate = () => {
    return courseList.some(
      (obj) =>
        obj.course === departmentAndCourse.course &&
        obj.department === departmentAndCourse.department &&
        obj.semester === semesterAndYear.semester &&
        obj.year === semesterAndYear.year
    );
  };
  const handleValidation = () => {
    let isValid = false;
    if (userInput !== '') {
      const arr = userInput.split(' '); //splitting the string on space
      if (arr.length >= 2 && arr.length <= 4) {
        switch (arr.length) {
          case 2:
            if (validateDepartmentAndCourse(arr[0]) && validateSemesterAndYear(arr[1])) {
              isValid = true;
            }
            break;
          case 3:
            if (validateDepartmentAndCourse(arr[0], arr[1]) && validateSemesterAndYear(arr[1], arr[2])) {
              isValid = true;
            }
            break;
          case 4:
            if (validateDepartmentAndCourse(arr[0], arr[1]) && validateSemesterAndYear(arr[2], arr[3])) {
              isValid = true;
            }
            break;
        }
      }
    }

    return isValid;
  };

  //Had to lookup event type
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (handleValidation()) {
      setIsValidAndNormalised(true);
      setError({ error: false, message: '' });
    } else {
      setIsValidAndNormalised(false);
      setError({ error: true, message: 'Could not parse course.' });
    }
  };

  // For clean user experience
  useEffect(() => {
    let isCancelled = false;
    if (userInput === '' && !isCancelled) {
      setIsValidAndNormalised(false);
      setError({ error: false, message: '' });
    }
    return () => {
      isCancelled = true;
    };
  }, [userInput]);

  useEffect(() => {
    let isCancelled = false;

    if (
      !isCancelled &&
      isValidAndNormalised &&
      departmentAndCourse.course !== '' &&
      departmentAndCourse.department !== '' &&
      semesterAndYear.semester !== '' &&
      semesterAndYear.year !== ''
    ) {
      if (!isDuplicate()) {
        setCourseList([
          {
            course: departmentAndCourse.course,
            department: departmentAndCourse.department,
            semester: semesterAndYear.semester,
            year: semesterAndYear.year,
          },
          ...courseList,
        ]);
      } else {
        setError({ error: true, message: 'is duplicate string.' });
      }
    }
    return () => {
      isCancelled = true;
    };
  }, [
    departmentAndCourse.course,
    departmentAndCourse.department,
    semesterAndYear.semester,
    semesterAndYear.year,
    isValidAndNormalised,
  ]);

  return (
    <div className='relative w-full'>
      <form onSubmit={handleSubmit}>
        <label className='block mb-2 text-sm font-medium'>Course</label>
        <div className='flex gap-2'>
          <input
            type='text'
            value={userInput}
            className={`text-sm rounded-sm p-2.5 border border-gray-300 w-full ${error.error ? 'border-red-600' : ''}`}
            placeholder='Enter Course'
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => setUserInput(event.target.value)}
          />
          <button
            type='submit'
            className='px-6 py-2.5 text-sm font-medium text-blue-800 bg-transparent border border-blue-800 rounded-sm cursor-pointer hover:bg-blue-800 hover:text-white hover:border-transparent disabled:bg-gray-300 disabled:text-gray-500 disabled:border-gray-300'
            disabled={!userInput}>
            Submit
          </button>
        </div>

        {error.error && error.message !== '' && (
          <p className='mt-2 text-sm text-red-600'>
            <span className='font-medium'>Error:</span> {error.message}
          </p>
        )}

        {courseList &&
          courseList.map((courseDetails: ICourseDetails, idx: number) => (
            <div key={idx} className='w-full mt-5 border border-gray-300 rounded-sm'>
              <div className='relative p-5 bg-blue-800'>
                <span className='font-medium text-white'>
                  {courseDetails.department} {courseDetails.course}
                </span>
              </div>
              <div className='p-5'>
                <div>
                  <label className='text-gray-500'>Department: </label>
                  <span>{courseDetails.department}</span>
                </div>
                <div>
                  <label className='text-gray-500'>Course: </label>
                  <span>{courseDetails.course}</span>
                </div>
                <div>
                  <label className='text-gray-500'>Year: </label>
                  <span>{courseDetails.year}</span>
                </div>
                <div>
                  <label className='text-gray-500'>Semester: </label>
                  <span>{courseDetails.semester}</span>
                </div>
              </div>
            </div>
          ))}
      </form>
    </div>
  );
}

export default CourseForm;
